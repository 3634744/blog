class Post < ActiveRecord::Base
has_many :comments, dependent: :destroy

validates_presence_of :title
validates_length_of :title, maximum: 50

validates_presence_of :body
validates_length_of :body, maximum: 200

end
