class Comment < ActiveRecord::Base
belongs_to :post

validates_presence_of :post_id
validates_numericality_of :post_id, :only_integer => true
validates_length_of :post_id, maximum: 2

validates_presence_of :body
validates_length_of :body, maximum: 200

end
